class Case {
  Case(this.abscisse, this.ordonne /*, this.multiLettre, this.multMot*/);

  int abscisse;
  int ordonne;

  Case.byDefault() : this(0, 0);

  bool isContent(Case c) =>((abscisse == c.abscisse) && (ordonne == c.ordonne))? true : false;
  
  @override
  String toString() => '(vide)';

  /// Cordonne case Mot triple
  List<Case> motTriple() => [
    Case(0, 0),
    Case(0, 7),
    Case(0, 14),
    Case(7, 0),
    Case(14, 0),
    Case(14, 7),
    Case(14, 14),
    Case(7, 14)
  ];

//----------------------------------------------------------------------------
  /// Mot double
  List<Case> motDouble() => [
    Case(1, 1),
    Case(2, 2),
    Case(3, 3),
    Case(4, 4),
    Case(1, 13),
    Case(2, 12),
    Case(3, 11),
    Case(4, 10),
    Case(7, 7),
    Case(10, 1),
    Case(11, 2),
    Case(12, 3),
    Case(13, 4),
    Case(10, 10),
    Case(11, 11),
    Case(12, 12),
    Case(13, 13)
  ];

//-------------------------------------------------------------------
  /// Lettre double
  ///
  List<Case> lettreDouble() => [
    Case(0, 3),
    Case(0, 11),
    Case(3, 14),
    Case(3, 0),
    Case(11, 0),
    Case(11, 14),
    Case(6, 2),
    Case(8, 2),
    Case(7, 3),
    Case(14, 3),
    Case(3, 6),
    Case(6, 6),
    Case(8, 6),
    Case(12, 6),
    Case(3, 7),
    Case(11, 7),
    Case(2, 8),
    Case(6, 8),
    Case(12, 8),
    Case(0, 11),
    Case(7, 11),
    Case(14, 11),
    Case(6, 12),
    Case(8, 12),
    Case(8, 8)
  ];

//-----------------------------------------------------------------------------------------
  /// Lettre triple

  List<Case> lettreTriple() => [
    Case(1, 5),
    Case(1, 9),
    Case(5, 1),
    Case(5, 5),
    Case(5, 9),
    Case(5, 13),
    Case(9, 1),
    Case(9, 5),
    Case(9, 9),
    Case(9, 13),
    Case(13, 5),
    Case(13, 9)
  ];
}

import 'case.dart';

/// Cordonne case Mot triple
Case mt1 = Case(0, 0);
Case mt2 = Case(0, 7);
Case mt3 = Case(0, 14);
Case mt4 = Case(7, 0);
Case mt5 = Case(14, 0);
Case mt6 = Case(14, 7);
Case mt7 = Case(14, 14);
Case mt8 = Case(7, 14);

final motTriple = <Case>{mt1, mt2, mt3, mt4, mt5, mt6, mt7, mt8};

//----------------------------------------------------------------------------
/// Mot double
Case md1 = Case(1, 1);
Case md2 = Case(2, 2);
Case md3 = Case(3, 3);
Case md4 = Case(4, 4);
Case md5 = Case(1, 13);
Case md6 = Case(2, 12);
Case md7 = Case(3, 11);
Case md8 = Case(4, 10);
Case md9 = Case(7, 7);
Case md10 = Case(10, 1);
Case md11 = Case(11, 2);
Case md12 = Case(12, 3);
Case md13 = Case(13, 4);
Case md14 = Case(10, 10);
Case md15 = Case(11, 11);
Case md16 = Case(12, 12);
Case md17 = Case(13, 13);

final motDouble = <Case>{
  md1,
  md2,
  md3,
  md4,
  md5,
  md6,
  md7,
  md8,
  md9,
  md10,
  md11,
  md12,
  md13,
  md14,
  md15,
  md16,
  md17
};

//-------------------------------------------------------------------
/// Lettre double
///
Case ld1 = Case(0, 3);
Case ld2 = Case(0, 11);
Case ld3 = Case(3, 14);
Case ld4 = Case(3, 0);
Case ld5 = Case(11, 0);
Case ld6 = Case(11, 14);
Case ld7 = Case(6, 2);
Case ld8 = Case(8, 2);
Case ld9 = Case(7, 3);
Case ld10 = Case(14, 3);
Case ld11 = Case(3, 6);
Case ld12 = Case(6, 6);
Case ld13 = Case(8, 6);
Case ld14 = Case(12, 6);
Case ld15 = Case(3, 7);
Case ld16 = Case(11, 7);
Case ld17 = Case(2, 8);
Case ld18 = Case(6, 8);
Case ld19 = Case(12, 8);
Case ld20 = Case(0, 11);
Case ld21 = Case(7, 11);
Case ld22 = Case(14, 11);
Case ld23 = Case(6, 12);
Case ld24 = Case(8, 12);
Case ld25 = Case(8, 8);

final lettreDouble = <Case>{
  ld1,
  ld2,
  ld3,
  ld4,
  ld5,
  ld6,
  ld7,
  ld8,
  ld9,
  ld10,
  ld11,
  ld12,
  ld13,
  ld14,
  ld15,
  ld16,
  ld17,
  ld18,
  ld19,
  ld20,
  ld21,
  ld22,
  ld23,
  ld24,
  ld25
};

//-----------------------------------------------------------------------------------------
/// Lettre triple
///
Case lt1 = Case(1, 5);
Case lt2 = Case(1, 9);
Case lt3 = Case(5, 1);
Case lt4 = Case(5, 5);
Case lt5 = Case(5, 9);
Case lt6 = Case(5, 13);
Case lt7 = Case(9, 1);
Case lt8 = Case(9, 5);
Case lt9 = Case(9, 9);
Case lt10 = Case(9, 13);
Case lt11 = Case(13, 5);
Case lt12 = Case(13, 9);

final lettreTriple = <Case>{
  lt1,
  lt2,
  lt3,
  lt4,
  lt5,
  lt6,
  lt7,
  lt8,
  lt9,
  lt10,
  lt11,
  lt12
};

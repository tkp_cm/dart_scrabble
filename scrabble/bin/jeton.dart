class Jeton {
  String lettre;
  int valeur;
  int quantite;

  Jeton({required this.lettre, required this.valeur, required this.quantite});
  Jeton.bydefault() : this(lettre: '', valeur: 0, quantite: 0);

  bool isJeton() => (quantite == 0) ? true : false;

  @override
  String toString() => lettre;
}

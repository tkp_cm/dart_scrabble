

List<Map<String, String>> alphabet = [
  {'lettre': 'A', 'quantite': '9','valeur': '1'}, 
  {'lettre': 'B', 'quantite': '2', 'valeur': '3'}, 
  {'lettre': 'C', 'quantite': '2', 'valeur': '3'}, 
  {'lettre': 'D', 'quantite': '3', 'valeur': '2'}, 
  {'lettre': 'E', 'quantite': '15', 'valeur': '1'}, 
  {'lettre': 'F', 'quantite': '2', 'valeur': '4'}, 
  {'lettre': 'G', 'quantite': '2', 'valeur': '2'}, 
  {'lettre': 'H', 'quantite': '2', 'valeur': '4'}, 
  {'lettre': 'I', 'quantite': '8', 'valeur': '1'}, 
  {'lettre': 'J', 'quantite': '1', 'valeur': '8'},
  {'lettre': 'K', 'quantite': '1', 'valeur': '10'},
  {'lettre': 'L', 'quantite': '5', 'valeur': '1'}, 
  {'lettre': 'M', 'quantite': '3', 'valeur': '2'}, 
  {'lettre': 'N', 'quantite': '6', 'valeur': '1'}, 
  {'lettre': 'O', 'quantite': '6', 'valeur': '1'}, 
  {'lettre': 'P', 'quantite': '2', 'valeur': '3'},
  {'lettre': 'Q', 'quantite': '1', 'valeur': '8'},
  {'lettre': 'R', 'quantite': '6', 'valeur': '1'}, 
  {'lettre': 'S', 'quantite': '6', 'valeur': '1'}, 
  {'lettre': 'T', 'quantite': '6', 'valeur': '1'},
  {'lettre': 'U', 'quantite': '6', 'valeur': '1'},
  {'lettre': 'V', 'quantite': '2', 'valeur': '4'},
  {'lettre': 'W', 'quantite': '1 ', 'valeur': '10'},
  {'lettre': 'X', 'quantite': '1', 'valeur': '10'},
  {'lettre': 'Y', 'quantite': '1', 'valeur': '10'},
  {'lettre': 'Z', 'quantite': '1', 'valeur': '10'},
  {'lettre': '*', 'quantite': '2', 'valeur': '0'}
];


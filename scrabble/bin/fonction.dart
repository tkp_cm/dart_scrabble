import 'dart:io';

import 'chevalet.dart';
import 'joueur.dart';
import 'plateau.dart';
import 'sac.dart';

void menuPrincipal() {
  Plateau plateau = Plateau();
  Sac sac = Sac();
  sac.remplir();
  print('--------------------------------------------------------------------');
  print('---                                                              ---');
  print('---                       Scrabble 1.0                           ---');
  print('---                                                              ---');

  print(
      '--------------------------------------------------------------------\n');
  print('--- Nombre de participant fixe a 2 pour le moment');

  print('--- Pseudo Joueur 1:');
  String? nom = stdin.readLineSync();
  Joueur joueur1 = Joueur(pseudo: nom!);

  print('--- Pseudo Joueur 2:');
  nom = stdin.readLineSync();
  Joueur joueur2 = Joueur(pseudo: nom!);

  Chevalet chevalet1 = Chevalet();
  chevalet1.remplirChevalet(sac: sac);

  Chevalet chevalet2 = Chevalet();
  chevalet2.remplirChevalet(sac: sac);

  infoJoueur(joueur1.pseudo, joueur1.points);
  print('--- Apres tirage, voici le contenu de votre chevalet:');
  print('--- $chevalet1');
  print('------------------------------------------------------');
  print('---');
  print('--- ${joueur1.pseudo} Entrer votre mot en fonction de vos letres');
  nom = stdin.readLineSync();
  print('Donner le sens de placement de votre mot: H pour horizontal et V pour vertical');
  String? nw = stdin.readLineSync();

}

void infoJoueur(String nom, int score) {
  print('--- Pseudo: $nom');
  print("---Point: $score");
}

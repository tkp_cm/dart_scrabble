import 'case.dart';
import 'case_bonus.dart';
import 'enble_jeton.dart';
import 'plateau.dart';

int pointSimple({required String mot}) {
  int resultat = 0;
  for (int i = 0; i < mot.length; i++) {
    resultat += valeurLettre(lettre: mot[i]);
  }
  return resultat;
}

// fontion de test si la valeur de la lettre sera double
bool estLettreDouble({required Case caseTest}) {
  for (Case element in lettreDouble) {
    if ((element.abscisse == caseTest.abscisse) &&
        (element.ordonne == caseTest.ordonne)) {
      return true;
    }
  }
  return false;
}

/// Fonction de test si la valeur de la lettre sera triple
bool estLettreTriple({required Case caseTest}) {
  for (Case element in lettreTriple) {
    if ((element.abscisse == caseTest.abscisse) &&
        (element.ordonne == caseTest.ordonne)) {
      return true;
    }
  }
  return false;
}

/// test si le total des point du mot sera triple
bool estMotTriple({required Case caseTest}) {
  for (Case element in motTriple) {
    if ((element.abscisse == caseTest.abscisse) &&
        (element.ordonne == caseTest.ordonne)) {
      return true;
    }
  }
  return false;
}

// teste si la valeur total du mot sera double
bool estMotDouble({required Case caseTest}) {
  for (Case element in motDouble) {
    if ((element.abscisse == caseTest.abscisse) &&
        (element.ordonne == caseTest.ordonne)) {
      return true;
    }
  }
  return false;
}

int valeurLettre({required String lettre}) {
  var tab = [];
  int res = 0;
  for (int i = 0; i < alphabet.length; i++) {
    alphabet[i].forEach((key, value) {
      tab.add(value);
    });
    if (lettre.toUpperCase().contains(tab[0])) {
      res = int.parse(tab[2]);
    }

    tab.clear(); //vider le tableau pour mettre le jeton suivant
  }
  return res;
}

// fonction de calcul des points des pour un joueur
int pointMot(
    {required List<List<String>> carreaux,
    required String mot,
    required String sens,
    required Case caseDeDepart}) {
  int val = 0, mult = 1;
  mot = mot.toUpperCase();
  switch (sens.toUpperCase()) {
    case 'H':
      for (int i = 0; i < mot.length; i++) {
        if (estLettreDouble(caseTest: caseDeDepart)) {
          val = valeurLettre(lettre: mot[i]);
        } else if (estLettreTriple(caseTest: caseDeDepart)) {
          val = (valeurLettre(lettre: mot[i]) * 2);
        } else if (estMotDouble(caseTest: caseDeDepart)) {
          mult = 2;
        } else if (estMotTriple(caseTest: caseDeDepart)) {
          mult = 3;
        } 
        caseDeDepart.abscisse++;
      }
      return (val + pointSimple(mot: mot)) * mult;

    case 'V':
      for (int i = 0; i < mot.length; i++) {
        if (estLettreDouble(caseTest: caseDeDepart)) {
          val = valeurLettre(lettre: mot[i]);
        } else if (estLettreTriple(caseTest: caseDeDepart)) {
          val = (valeurLettre(lettre: mot[i]) * 2);
        } else if (estMotDouble(caseTest: caseDeDepart)) {
          mult = 2;
        } else if (estMotTriple(caseTest: caseDeDepart)) {
          mult = 3;
        } 
        caseDeDepart.ordonne++;
      }
      return (val + pointSimple(mot: mot)) * mult;
    default:
      return 0;
  }
}

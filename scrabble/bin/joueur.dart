import 'enble_jeton.dart';
import 'jeton.dart';
import 'mot_valide.dart';
import 'chevalet.dart';

class Joueur {
  String pseudo;
  int points;

  Joueur({required this.pseudo}) : points = 0;

  //test si le mot est valide
  bool estValide({required String mot}) => motValide.contains(mot);

  int pointSimple({required String mot}) {
    var tab = [];
    int resultat = 0;
    for (int i = 0; i < alphabet.length; i++) {
      alphabet[i].forEach((key, value) {
        tab.add(value);
      });
      resultat += int.parse(tab[2]);
      tab.clear(); //vider le tableau pour mettre le jeton suivant
    }
    return resultat;
  }

  String poserMot({
    required String mot,
    required Chevalet chevalet,
  }) {
    mot = mot.toLowerCase();
    if (motValide.contains(mot)) {
      var indexSup = [];
      for (int i = 0; i < mot.length; i++) {
        if (mot[i].toUpperCase().contains(chevalet.chevalet[i].lettre)) {
          print(chevalet.chevalet[i].lettre);
          int index = chevalet.chevalet.indexOf(chevalet.chevalet[i]);
          indexSup.add(index);
        }
      }
      for (var elt in indexSup) {
        chevalet.chevalet.removeAt(elt);
      }
    } else {
      throw Exception('mot invalid');
    }
    return mot;
  }

  @override
  String toString() => '$pseudo\n$points';

//
}

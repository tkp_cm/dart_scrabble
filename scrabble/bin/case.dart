class Case {
  Case( this.abscisse, this.ordonne);

  Case.bydefault() : this(0,0);

  int abscisse;
  int ordonne;
  //int valeur;


  @override
  String toString() => '[$abscisse - $ordonne]';
}

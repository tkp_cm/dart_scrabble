import 'case.dart';
import 'case_bonus.dart';

class Plateau {
  List<List<String>> carreaux = List.generate(
      15,
      (index) => [
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*',
            '*'
          ]);

  void affichage() {
    for (int i = 0; i < carreaux.length; i++) {
      print(carreaux[i]);
    }
  }

  // insertion d'un mot valide sur le plateau
  List<List<String>> recevoirMot(
      {required String mot, required String sens, required Case caseDeDepart}) {
    switch (sens.toUpperCase()) {
      case 'H':
        for (int i = 0; i < mot.length; i++) {
          carreaux[caseDeDepart.abscisse][caseDeDepart.ordonne] =
              mot[i].toUpperCase();

          caseDeDepart.abscisse++;
        }
        return carreaux;
      case 'V':
        for (int i = 0; i < mot.length; i++) {
          carreaux[caseDeDepart.abscisse][caseDeDepart.ordonne] =
              mot[i].toUpperCase();
          caseDeDepart.ordonne++;
        }
        return carreaux;
      default:
        return carreaux;
    }
  }


}

import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart' as p;
import 'package:test/test.dart';
import 'case.dart';
import 'case_bonus.dart';
import 'chevalet.dart';
import 'enble_jeton.dart';
import 'fonction.dart';
import 'joueur.dart';
import 'plateau.dart';
import 'sac.dart';
import 'total_point.dart';

void main() async {
  
  //print(creationTableau());

  Plateau plateau = Plateau();
  Sac sac = Sac();
  sac.remplir();
  print(sac);

  //print(sac.prendreJeton());

  //print(sac);

  Chevalet chevalet = Chevalet();
  chevalet.remplirChevalet(sac: sac);
  print(chevalet);

  Joueur j1 = Joueur(pseudo: 'tkp');

  plateau.carreaux = plateau.recevoirMot(
      mot: j1.poserMot(mot: 'abandon', chevalet: chevalet),
      sens: 'h',
      caseDeDepart: Case(7, 7));

  plateau.affichage();

  int po = pointMot(
      carreaux: plateau.carreaux,
      mot: 'abandon',
      sens: 'H',
      caseDeDepart: Case(7, 7));
  print('point du mot: $po');

    plateau.carreaux = plateau.recevoirMot(
      mot: j1.poserMot(mot: 'abasie', chevalet: chevalet),
      sens: 'v',
      caseDeDepart: Case(9, 5));

  plateau.affichage();

  po += pointMot(
      carreaux: plateau.carreaux,
      mot: 'abasie',
      sens: 'v',
      caseDeDepart: Case(9, 5));
  print('point du mot: $po');

}

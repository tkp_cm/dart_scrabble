import 'sac.dart';

import 'jeton.dart';

class Chevalet {
  List<Jeton> chevalet;

  Chevalet() : chevalet = [];

  void remplirChevalet({required Sac sac}) {
    for (int i = 0; i < 7; i++) {
      Jeton j = sac.prendreJeton();
      chevalet.add(j);
    }
    sac.taille = sac.totalSac(sac: sac); // cacul des jetos restant dans le sac
  }

  void viderChevalet({required Sac sac}) {
    for (int i = 0; i < chevalet.length; i++) {
      //parcour du chevalet
      if (sac.contennu.contains(chevalet[i])) {
        // test si la lettre se trouve dans le sac
        for (int j = 0; j < sac.contennu.length; j++) {
          // parcour du sac
          if (chevalet[i].lettre == sac.contennu[j].lettre) {
            sac.contennu[j].quantite++; // augmentation de la quantite
          }
        }
      } else {
        // si la lettre n'existe pas on ajoute simplement
        chevalet[i].quantite = 1;
        sac.contennu.add(chevalet[i]);
      }
    }
    sac.taille = sac.totalSac(sac: sac); // cacul des jetos restant dans le sac
    chevalet.clear();
  }

  @override
  String toString() => 'Chevalet: ${chevalet.toString()}';
}

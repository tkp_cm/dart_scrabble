import 'dart:math';
import 'enble_jeton.dart';
import 'jeton.dart';

class Sac {
  List<Jeton> contennu;
  int taille;

  Sac()
      : contennu = [],
        taille = 0;

  int totalSac({required Sac sac}) {
    int resultat = 0;
    for (int i = 0; i < contennu.length; i++) {
      resultat += contennu[i].quantite;
    }
    return resultat;
  }

  //fonction permettant de remplir le sac de jeton
  void remplir() {
    var tab = [];
    for (int i = 0; i < alphabet.length; i++) {
      alphabet[i].forEach((key, value) {
        tab.add(value);
      });
      taille += int.parse(tab[1]);
      contennu.add(Jeton(
          lettre: tab[0],
          valeur: int.parse(tab[2]),
          quantite: int.parse(tab[1])));

      tab.clear(); //vider le tableau pour mettre le jeton suivant
    }
  }

  // fonction permettant de piocher un jeton au hazards dans le sac
  Jeton prendreJeton() {
    Jeton choix = contennu[Random().nextInt(contennu.length)];
    choix.quantite -= 1;
    taille--;
    contennu.removeWhere((choix) =>
        choix.quantite ==
        0); // si quantite == 0, on supprime le jeton de notre sac
    return choix;
  }

  @override
  String toString() =>
      'Taille:$taille\n Sac:\n${contennu.toString().toString()}';
}
